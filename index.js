// CRUD Operations (Create, Read, Update, Delete)
/*
	- CRUD operations are the heart of any backend application
	- Mastering the CRUD operations is essential for any developer
	- This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
	- Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
*/

// [SECTION] Inserting Documents (Create)
/*
	Syntax:
		- db.collectionName.insertOne({object});
*/

// Insert One - inserts one document
db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "87654321",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// Insert Many - inserts one or more documents
/*
	Syntax
		- db.collectionName.insertMany([{objectA}, {objectB}]);
*/

db.users.insertMany(
	[
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:{
				phone: "87000",
				email: "stephenhawking@mail.com"
			},
			courses: ["Python", "React", "Php"],
			department: "none"			
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:{
				phone: "4770220",
				email: "neilarmstrong@mail.com"
			},
			courses: ["React", "Laravel", "MongoDB"],
			department: "none"			
		}
	]
)

// [SECTION] Finding Documents (Read)

/*
	- db.collectionName.find();
	- db.collectionName.find({field: value});
*/

// retrieves all documents
db.users.find();

// retrieves specific document
db.users.find({firstName: "Stephen"});

// multiple criteria
db.users.find({lastName: "Armstrong", age:82});
// fetch = 0
db.users.find({lastName: "Armstrong", age:70});


// [SECTION] Update and Replace (UPDATE)

// Inserting something we want to update
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});

/*
	Syntax
		db.collectionName.updateOne({criteria}, {$set: {field/s : value/s}})
		db.collectionName.updateMany({criteria}, {$set: {field/s : value/s}})
		db.collectionName.replaceOne({criteria}, {field/s : value/s})
		db.collectionName.replaceMany({criteria}, {field/s : value/s})
*/

db.users.updateOne(
	{ firstName: "Test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "iamrich@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)

db.users.find();

db.users.updateOne(
	{ firstName: "Jane" },
	{
		$set: {
			firstName: "Jenny"
		}
	}
)

db.users.find();

db.users.updateMany(
	{ department: "none" },
	{
		$set: {
			department: "HR"
		}
	}
)

db.users.find();

db.users.replaceOne(
	{firstName: "Bill"}, // criteria
	{
		firstName: "Elon",
		lastName: "Musk",
		age: 30,
		courses: ["PHP", "Laravel", "HTML"],
		status: "trippings"
	}
)


// [SECTION] Delete
/*
	Syntax
		db.collectionName.deleteOne({field: value});
		db.collectionName.deleteMany({field: value});
*/

// deletes one document that matches the field and value
db.users.deleteOne({
	firstName: "Jane"
})

// deletes all documents that satisfies the field and value
db.users.deleteMany({
	firstName: "Neil"
})

db.users.find();

db.users.deleteMany({
	department: "HR"
})